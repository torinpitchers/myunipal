//
//  HotDealsTableView.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 02/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import Foundation
import UIKit
import CoreData




class HotDealsTable: UITableViewController {
    
    
    @IBAction func buttonpressed(sender: AnyObject) {
        do{
            let deal:NSManagedObject = try Deals.getInstance.fetchDeal(sender.tag)
        let link:String = deal.valueForKey("link") as! String
        let url:NSURL = NSURL(string: link)!

        
    
            UIApplication.sharedApplication().openURL(url) }
        catch{}
  
       
        
        
    }
    
    func reload(notification: NSNotification){
        //load data here
        self.tableView.reloadData()
    }
    
    func update(sender:AnyObject)
    {
        do {
            Deals.getInstance.clear()
            try Deals.getInstance.load(6)}
        catch{}
        sleep(10)
        self.tableView.reloadData()
        
        sender.endRefreshing()
    }
        
    override func viewDidLoad() {
        print("loaded")
        self.title = "Hot Deals"
        
        //add notification for reloading data
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reload:",name:"reload", object: nil)
        
        
        //set default
        DealCell.getInstance.initial()
        
        //refreshing
        
        var refreshControl:UIRefreshControl!
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update")
        refreshControl.addTarget(self, action: "update:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        
        

        
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Deals.getInstance.count()
    }
    
    
    
    
    
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("deal_cell", forIndexPath: indexPath) as! DealCell
            
            cell.title.hidden = true
            cell.price.hidden = true
            cell.deal_image.hidden = true
            cell.date.hidden = true
            cell.temp.hidden = true
            cell.button.hidden = true
            cell.from.hidden = true
            cell.list.hidden = false
        }
        
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("deal_cell", forIndexPath: indexPath) as! DealCell
        
        do{
            let deals:NSManagedObject = try Deals.getInstance.fetchDeal(indexPath.row)
        
        
        if let title = cell.title {
            let titleString = deals.valueForKey("title") as? String
            title.text = titleString
        }
        if let price = cell.price {
            let value:Float = deals.valueForKey("price") as! Float
            let price_string:String = String(format:"%.2f", value)
            let string:String = "£" + price_string
            price.text = string
        }
        if let image = cell.deal_image {
            image.contentMode = .ScaleAspectFit
            image.image = UIImage(data: deals.valueForKey("image") as! NSData)
        }
        if let dater = cell.date {
            let stamp:Int = deals.valueForKey("timestamp_date") as! Int
            let timeInt:NSTimeInterval = NSTimeInterval(stamp)
            let date:NSDate = NSDate(timeIntervalSince1970: timeInt)
            let formatter:NSDateFormatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            formatter.timeStyle = NSDateFormatterStyle.MediumStyle
            let dateString:String = formatter.stringFromDate(date) 
            
            dater.text = dateString
        }
        if let temp = cell.temp {
            let temperature:Float = deals.valueForKey("temperature") as! Float
            if temperature >= 1000 {
                temp.text = "HOT"
            }
            else {
                temp.text = ""
            }
        }
        if let from = cell.from {
            let merchant:String = deals.valueForKey("merchant_name") as! String
            from.text = "From " + merchant
        }
        if let button = cell.button {
            button.tag = indexPath.row
        }
            //no highlight on click
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
       
        }
    catch {}
        return cell
    }
}