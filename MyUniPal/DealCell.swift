//
//  DealCell.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 03/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import UIKit
import Foundation



class DealCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var deal_image: UIImageView!
    
    @IBOutlet weak var from: UILabel!
    
    @IBOutlet weak var temp: UILabel!
    
    @IBOutlet weak var list: UIPickerView!
    
    // static class public property that returns a Budget object
    static let getInstance = DealCell()
    
    private let options:[String] = [ "Price Low - High", "Price High - low", "Newest Deals"]
    
    
    func initial() {
        //default selected
        //self.list.selectRow(2, inComponent: 0, animated: true)
        Deals.getInstance.reorder("timestamp_date", ascending: false)
        
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label:UILabel = UILabel()
        label.font = UIFont.systemFontOfSize(UIFont.systemFontSize()*3)
        label.textAlignment = NSTextAlignment.Center
        label.text = options[row]
        label.layer.backgroundColor = ConsistentColor.CGColor
        
        return label
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        
        if row == 0 {
            list.selectRow(0, inComponent: 0, animated: true)
            Deals.getInstance.reorder("price", ascending: true)
            
            
        }
        else if row == 1 {
            list.selectRow(1, inComponent: 0, animated: true)
            Deals.getInstance.reorder("price", ascending: false)
        }
        else if row == 2 {
            list.selectRow(2, inComponent: 0, animated: true)
            Deals.getInstance.reorder("timestamp_date", ascending: false)
        }
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("reload", object: nil)
        
        
        
        
        
        
    }
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.list.delegate = self
        self.list.dataSource = self
        
        title.font = UIFont.systemFontOfSize(UIFont.systemFontSize()*1.5)
        
        date.font = UIFont.systemFontOfSize(UIFont.systemFontSize() * 0.6)
        date.textColor = UIColor.lightGrayColor()
        
        price.font = UIFont(name: "AvenirNextCondensed-Medium", size: UIFont.systemFontSize() * 2)
        price.textColor = UIColor(red: 0x33 / 255.0, green: 0xcc / 255.0, blue: 0x33 / 255.0, alpha: 1)
        
        temp.font = UIFont.boldSystemFontOfSize(UIFont.systemFontSize() * 1.4)
        temp.textColor = UIColor(red: 0xff / 255.0, green: 0x00 / 255.0, blue: 0x00 / 255.0, alpha: 0.3)
        
        from.font = UIFont.systemFontOfSize(UIFont.systemFontSize() * 0.6)
        from.textColor = UIColor(red: 0x33 / 255.0, green: 0xcc / 255.0, blue: 0x33 / 255.0, alpha: 0.5)
        
        
        
    
        button.layer.backgroundColor = ConsistentColor.CGColor
        button.setTitle("Go", forState: UIControlState.Normal)
        
        
        list.hidden = true
        list.selectRow(2, inComponent: 0, animated: true)
        
        
        
        
        
    }

    

}
