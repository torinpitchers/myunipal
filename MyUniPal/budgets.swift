//
//  budgets.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 09/11/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import Foundation
import CoreData
import UIKit


//define data type of a budget
struct BudgetType {
    var name:String
    var amount_week:Float
    var amount_month:Float
    var deductions:Float
    var increments:Float
    var week:Bool
    var month:Bool
    var active:Bool
    var activedate:NSDate
    
}
//errors for error handelling
enum BudgetError: ErrorType {
    case NameTaken
    case InvlaidAmount
    case BadIndex
    case NoTimeFrame
    case InvalidName
    case InvalidBudget
    case ActiveNotFound
    case ActiveAlreadyFalse
    case FailedToCalculateBudget
    
}


/// The **Budget** class handles the creation and maintaining of a list for Budgets.
///
/// Overview
///
/// A Budget object provides support for the following activities:
///
/// 1. adding new Budgets
/// 2. retrieving Budgets based on their name
/// 3. changing the active budget when the new budget added is to be active
/// 4. deleting a budget from the list
/// 5. retrieveing a budget from the list
class Budget: NSObject, NSFetchedResultsControllerDelegate {
    
    //variables used when creating a new budget
    var name:String?
    var amount_week:Float?
    var amount_month:Float?
    var deductions:Float?
    var increments:Float?
    var week:Bool?
    var month:Bool?
    var active:Bool?
    var activedate:NSDate?
    
    
    private var BudgetList:[BudgetType]
    
    // static class public property that returns a Budget object
    static let getInstance = Budget()
    
    /// creates a new Budget object from within the class only
    private override init() {
        // initialises the empty array
        self.BudgetList = []
        
        //creates variables that are used to store a new budget to be created
        
        
    }
    
    var managedObjectContext: NSManagedObjectContext?
    var fetchedResults: NSFetchedResultsController?
    
    /* the NSFetchedResultsController manages the results from a Core Data fetch request. */
    var fetchedResultsController: NSFetchedResultsController {
        if self.fetchedResults != nil {
            return self.fetchedResults!
        }
        let managedObjectContext = self.managedObjectContext!
        /* the NSEntityDescription describes one of the Core Data entities. */
        let entity = NSEntityDescription.entityForName("Budget", inManagedObjectContext: managedObjectContext)
        /* we want to sort on two keys, each defined as an NSSortDescriptor */
        let sortTitle = NSSortDescriptor(key: "name", ascending: true)
        /* Now we create am NSFetchRequest object */
        let req = NSFetchRequest()
        /* we assign our defined entity */
        req.entity = entity
        /* and an array of sort descriptors. */
        req.sortDescriptors = [sortTitle]
        /* the NSFetchedResultsController manages the search results, we use this in the table delegate and data source methods. The 'sectionNameKeyPath is the field we want to use for the table sections. */
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: req, managedObjectContext: managedObjectContext, sectionNameKeyPath: "name", cacheName: nil)
        aFetchedResultsController.delegate = self
        self.fetchedResults = aFetchedResultsController
        do {
            /* finally we perform the fetch. */
            try self.fetchedResults?.performFetch()
        } catch {
            print("failed to fetch data")
        }
        return self.fetchedResults!
    }
    
    
    

    /// calculates how many days are <b>left</b> in the current month.
    /// - returns: Int: how many days are left this month
    func daysleftmonth() -> Int{
        // todays date
        let today:NSDate = NSDate()
        //calendar object
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        //calculate total days in current month
        let range:NSRange = calendar.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: today)
        
        //calculate days passed in current month
        let daycomponent = calendar.component(NSCalendarUnit.Day, fromDate: today)
        var daysleft = range.length - daycomponent
        
        if daysleft == 0 {
            daysleft = 1
        }
        //return the amount of days left in the current month
        return daysleft
    }
    
    /// calculates how many weeks are <b>left</b> in the current month.
    /// - returns: Int: how many weeks are left this month
    func weeksleftmonth() -> Int{
        //get days left in month
        let daysleft = self.daysleftmonth()
        //divide by 7
        let weeksleft = daysleft / 7
        //if the value was an exact week then return the calculation
        if (daysleft == 7) || (daysleft == 14) || (daysleft == 21) || (daysleft == 28){
            return weeksleft
        }
        else {
            
            return weeksleft + 1
        }
        
    }
    
    /// calculates how many days <b>exist</b> in the current month.
    /// - returns: Int: how many days exist in this month
    func daysinmonth() -> Int{
        // todays date
        let today:NSDate = NSDate()
        //calendar object
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        //calculate total days in current month
        let range:NSRange = calendar.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: today)
        
        //return the amount of days left in the current month
        return range.length
    }
    
    /// calculates how many hours are <b>left</b> in the current day.
    /// - returns: Int: how many dhours are left today
    func hoursinday() -> Int{
        // todays date
        let today:NSDate = NSDate()
        //calendar object
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        //calculate days passed in current month
        let component = calendar.component(NSCalendarUnit.Hour, fromDate: today)
        
        //return the amount of days left in the current month
        return 24 - component
    }

    /// fetches the data from core data, usualy done before caling <b>loadArray()</b>
    func fetch() {
        do {
            /* when the view appears we need to retrieve the latest set of data but this may throw an error. */
            try self.fetchedResultsController.performFetch()
        } catch {
            print("data not reloading")
        }
    }
    
    /// loads the data from coredata into a local list called BudgetList
    func loadArray() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext

        
        //updating array with budgets from coredata
        
        //populate array with the data
        var name:String?
        var amount_week:Float?
        var amount_month:Float?
        var deductions:Float?
        var increments:Float?
        var week:Bool?
        var month:Bool?
        var active:Bool?
        var activedate:NSDate?
        
        let info = self.fetchedResultsController.fetchedObjects?.count
        
        Budget.getInstance.clear()
        for var index = 0; index < info; index++ {
            if info == 0 {
                break
            }
            let result = self.fetchedResultsController.fetchedObjects![index]
            if let BudgetName = result.valueForKey("name") as? String {
                name = BudgetName
            }
            if let BudgetAmountWeek = result.valueForKey("amount_week") as? Float {
                amount_week = BudgetAmountWeek
            }
            if let BudgetAmountMonth = result.valueForKey("amount_month") as? Float {
                amount_month = BudgetAmountMonth
            }
            if let BudgetDeductions = result.valueForKey("deductions") as? Float {
                deductions = BudgetDeductions
            }
            if let BudgetIncrements = result.valueForKey("increments") as? Float {
                increments = BudgetIncrements
            }
            if let BudgetWeek = result.valueForKey("week") as? Bool {
                week = BudgetWeek
            }
            if let BudgetMonth = result.valueForKey("month") as? Bool {
                month = BudgetMonth
            }
            if let BudgetActive = result.valueForKey("active") as? Bool {
                active = BudgetActive
            }
            if let BudgetActiveDate = result.valueForKey("activedate") as? NSDate {
                activedate = BudgetActiveDate
            }
            let budget = BudgetType(name: name!, amount_week: amount_week!, amount_month: amount_month!, deductions: deductions!, increments: increments!, week: week!, month: month!, active: active!, activedate: activedate!)
            
            do {
                
                try Budget.getInstance.addBudget(budget)
                
            } catch {}
            
            
            
            
        }
    }
    
    /// saves a budget to core data. no need to add it to the BudgetList as this list is repopulated allways before use
    /// - parameters:
    ///   - `budgetType: The budget that is to be added to core data
    func saveBudget(budget:BudgetType) {
        print("saveBudget")
        /* before using core data you need to get a reference to a 'NSManagedObjectContext' which is used to work with managed objects before they get saved to disk. This can represent any entity in the data model. */
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        /* the NSEntityDescription links the entity definition from the data model to the NSManagedObject. */
        let entity = NSEntityDescription.entityForName("Budget", inManagedObjectContext: managedContext)
        /* the NSManagedObject implements the functionality required to create a Core Data model object. */
        let newBudget = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        /* now we can assign values to the data model object keys */
        newBudget.setValue(budget.name, forKey: "name")
        newBudget.setValue(budget.amount_week, forKey: "amount_week")
        newBudget.setValue(budget.amount_month, forKey: "amount_month")
        newBudget.setValue(budget.deductions, forKey: "deductions")
        newBudget.setValue(budget.increments, forKey: "increments")
        newBudget.setValue(budget.week, forKey: "week")
        newBudget.setValue(budget.month, forKey: "month")
        newBudget.setValue(budget.active, forKey: "active")
        newBudget.setValue(NSDate(), forKey: "activedate")
        do {
            /* finally we try to save the data model object. This may throw an error (a generic NSError). */
            try managedContext.save()
            self.fetch()
            self.loadArray()
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
    }

    
    /// switches the old active budget to off, after a new active budget is added to the list, this change is made to core data then the list is reloaded from coredata
    /// - parameters:
    ///   - Int: The index fo which budget needs to be switched off
    func PreviousActiveOff () {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
        let info = self.fetchedResultsController.fetchedObjects?.count
        for var index = 0; index < info; index++ {
            if info == 0 {
                break
            }
            let result = self.fetchedResultsController.fetchedObjects![index]
            result.setValue(false, forKey: "active")
            do {
                try self.managedObjectContext?.save()
                self.fetch()
                self.loadArray()
            } catch {}
        }
    }
    
    
    
    func reset(){
        self.fetch()
        self.loadArray()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
        
        
        
        for budget in self.fetchedResultsController.fetchedObjects! {
            budget.setValue(0, forKey: "increments")
            budget.setValue(0, forKey: "deductions")
        }
        do {
            try self.managedObjectContext?.save() }
        catch{}
        
        self.called = false
        self.called2 = false
        self.mcalled = false
        self.mcalled2 = false
    }
    
    
    
    
    
    
    func newactive(index: Int) throws{
        //if the index does not exist
        if index < 0 || index > BudgetList.count {
            //throw an error
            throw BudgetError.BadIndex
        }
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
        
        let budget = self.fetchedResultsController.fetchedObjects![index-1]
        budget.setValue(true, forKey: "active")
        budget.setValue(NSDate(), forKey: "activedate")
        try self.managedObjectContext?.save()
        self.fetch()
        self.loadArray()
        
    }
    
    
    /// converts a string to a Float.
    /// - parameters:
    ///   - String: The tex requiring conversion
    func stringtofloat(text: String) -> Float {
        if let integer:Float = Float(text) {
            return integer
        }
        return Float(0)
    }
    
    
    /// clears all budgets from the local array
    func clear() {
        self.BudgetList = []
    }
    
    /// clears all budgets from coredata
    func removedata() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext

        for budget in self.fetchedResultsController.fetchedObjects! {
            self.managedObjectContext?.deleteObject(budget as! NSManagedObject)
            do{
                try self.managedObjectContext!.save()
                self.fetch()
                self.loadArray()
            }
            catch {}
        }
        }
    
    
    /// returns the count of the budgets both local and coredata
    /// - returns: Int: the count of Budgets
    func count() -> Int{
        return self.BudgetList.count
    }
    
    
    
    /// Adds a new budget to the list, if another budget is allready active it is turned off
    /// - parameters:
    ///   - BudgetType: The budget which is to be added to the budget list
    /// - throws: A `BudgetError.InvaidAmount` error, if both amounts are below or equal to 0
    /// A `BudgetError.NameTaken` error, if the budget to be added has the same name as a previous budget
    func addBudget(budget:BudgetType) throws
    {
        if budget.amount_week <= 0 && budget.amount_month <= 0{
            throw BudgetError.InvlaidAmount
        }
        
        for existingbudget in self.BudgetList {
            if existingbudget.name == budget.name {
                throw BudgetError.NameTaken
            }
        } 
    
        self.BudgetList.append(budget)
        
    }
    
    /// deletes a budget from core data, no need to remove it from the list as the array will be refetched from core data
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    /// - throws: A `BudgetError.BadIndex` error, if the index does not exist for the BudgetList
    /// A `BudgetError.NameTaken` error, if the budget to be added has the same name as a previous budget
    func deleteBudget(index:Int) throws {
        //if the index does not exist
        if index < 0 || index > BudgetList.count - 1 {
            //throw an error
            throw BudgetError.BadIndex
        }
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext

        //store how many Budgets are stored in core data
        let info = self.fetchedResultsController.fetchedObjects?.count
        //for all Budgets in coredata
        for var index2 = 0; index2 < info; index2++ {
            //if no budgets exist
            if info == 0 {
                break
            }
            //store the current budget being checked
            let result = self.fetchedResultsController.fetchedObjects![index2]
            //variable to store the name of the current budget
            var name:String?
            
            //store the name of the current budget
            if let BudgetName = result.valueForKey("name") as? String {
                name = BudgetName
            }
            //if the name of the curent budget from coredata is equal to the name of the budget in stored in the list
            if BudgetList[index].name   ==  name {
                //delete the Buget from coredata
                self.managedObjectContext?.deleteObject(result as! NSManagedObject)
                do {
                    //save
                    try self.managedObjectContext?.save()
                    //self.fetch()
                    //self.loadArray()
                } catch {}
            }
            
        }
    }

    
    /// retrieves a budget from BudgetList.
    /// - parameters:
    ///   - Int: The index for the budget you want to retrieve
    /// - throws: A `BudgetError.BadIndex` error, if the index does not exist for the BudgetList
    /// - returns: BudgetType: A Budget from the BudgetList
    func getBudget(ByIndex index:Int) throws -> BudgetType{
        if index < 0 || index > BudgetList.count - 1 {
            throw BudgetError.BadIndex
        }
        return self.BudgetList[index]
    }
    
    
    /// retrieves the active budget from BudgetList.
    /// - throws: A `BudgetError.ActiveNotFound` error, if the active budget could not be found (no active budget is set).
    /// - returns: Int: the index for the active budget stored in BudgetList
    func getBudgetActiveIndex() throws -> Int {
        
        var indexcounter:Int = -1
        for existingbudget in self.BudgetList {
            indexcounter += 1
            if existingbudget.active == true {
                return indexcounter
            }
            
            
        }
        throw BudgetError.ActiveNotFound
        
    }
    
    
    
    //////////forDay - by week///////
    var activechangedday:Bool?
    //day
    var called:Bool?
    var storeday:Float?
    var olddeduc:Float?
    var oldincre:Float?
    var days:Int = 0
    
    ////////forDay - byMonth ////////
    var mcalled:Bool?
    var mstoreday:Float?
    var molddeduc:Float?
    var moldincre:Float?
    var mdays:Int = 0
    
    //////////forweek - by week///////
    var activechangedweek:Bool?
    //day
    var called2:Bool?
    var storeweek:Float?
    var olddeduc2:Float?
    var oldincre2:Float?
    var weeks:Int = 0
    
    ////////forweek - byMonth ////////
    var mcalled2:Bool?
    var mstoreweek:Float?
    var molddeduc2:Float?
    var moldincre2:Float?
    var mweeks:Int = 0

    
    /// returns the Budget amount for the current day for a budget, 99% of the time this will be for the active budget
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    /// - throws: A `BudgetError.FailedToCalculateBudget` error, if the budget could not be calculated
    /// - returns: Int: the calculated amount per day for the budget
    func getBudgetAmount(ForDay index:Int) throws -> Float{
        //if budget is set as weeksly
        if self.BudgetList[index].week == true {
            //if the active budget has been changed
            if activechangedday == true {
                called = false
                activechangedday = false
            }
            //if a day has passed
            if days > self.daysleftmonth(){
                called = false
            }
            //if amount has allready been produced
            if called == true {
                //if changes have been made to deductions
                if (olddeduc != self.BudgetList[index].deductions) || (oldincre != self.BudgetList[index].increments) {
                    //return new amount for the day
                    return ((storeday! + olddeduc! - oldincre!)) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)
                }
                
                return storeday!
            }
            else {
                called = true
                try olddeduc = self.getBudget(ByIndex: self.getBudgetActiveIndex()).deductions
                try oldincre = self.getBudget(ByIndex: self.getBudgetActiveIndex()).increments
                storeday = ((((self.BudgetList[index].amount_week * 4) ) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)) / Float(self.daysleftmonth()))
                days = self.daysleftmonth()
                return storeday!
            }
            
        }
        else if self.BudgetList[index].month == true{
            if activechangedday == true {
                called = false
                activechangedday = false
            }
            if mdays > self.daysleftmonth(){
                mcalled = false
            }
            if mcalled == true {
                
                if (molddeduc != self.BudgetList[index].deductions) || (moldincre != self.BudgetList[index].increments) {
                    
                    return ((mstoreday! + molddeduc! - moldincre!)) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)
                }
                
                return mstoreday!
            }
            else {
                mcalled = true
                try molddeduc = self.getBudget(ByIndex: self.getBudgetActiveIndex()).deductions
                try moldincre = self.getBudget(ByIndex: self.getBudgetActiveIndex()).increments
                mstoreday = (((self.BudgetList[index].amount_month) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)) / Float(self.daysleftmonth()))
                mdays = self.daysleftmonth()
                return mstoreday!
            }
        }
        throw BudgetError.FailedToCalculateBudget
    }
    
    
    /// returns the Budget amount for the current week for a budget, 99% of the time this will be for the active budget
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    /// - throws: A `BudgetError.FailedToCalculateBudget` error, if the budget could not be calculated
    /// - returns: Int: the calculated amount per week for the budget
    func getBudgetAmount(ForWeek index:Int)  throws -> Float{
        if self.BudgetList[index].week == true {
            
            //if the active budget has been changed
            if activechangedweek == true {
                called2 = false
                activechangedweek = false
            }
            //if a day has passed
            if weeks > self.weeksleftmonth(){
                called2 = false
            }
            //if amount has allready been produced
            if called2 == true {
                //if changes have been made to deductions
                if (olddeduc2 != self.BudgetList[index].deductions) || (oldincre2 != self.BudgetList[index].increments) {
                    //return new amount for the day
                    return ((storeweek! + olddeduc2! - oldincre2!)) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)
                }
                
                return storeweek!
            }
            else {
                called2 = true
                try olddeduc2 = self.getBudget(ByIndex: self.getBudgetActiveIndex()).deductions
                try oldincre2 = self.getBudget(ByIndex: self.getBudgetActiveIndex()).increments
                storeweek = (((self.BudgetList[index].amount_week * 4) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)) / Float(weeksleftmonth()))
                weeks = self.weeksleftmonth()
                return storeweek!
            }
            
        }

        else if self.BudgetList[index].month == true{
            
            print("i have left")
            print(weeksleftmonth())

            
            //if the active budget has been changed
            if activechangedweek == true {
                mcalled2 = false
                activechangedweek = false
            }
            //if a day has passed
            if mweeks > self.weeksleftmonth(){
                mcalled2 = false
            }
            //if amount has allready been produced
            if mcalled2 == true {
                //if changes have been made to deductions
                if (molddeduc2 != self.BudgetList[index].deductions) || (moldincre2 != self.BudgetList[index].increments) {
                    //return new amount for the day
                    return ((mstoreweek! + molddeduc2! - moldincre2!)) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)
                }
                
                return mstoreweek!
            }
            else {
                mcalled2 = true
                try molddeduc2 = self.getBudget(ByIndex: self.getBudgetActiveIndex()).deductions
                try moldincre2 = self.getBudget(ByIndex: self.getBudgetActiveIndex()).increments
                mstoreweek = (((self.BudgetList[index].amount_month) + (self.BudgetList[index].increments - self.BudgetList[index].deductions)) / Float(weeksleftmonth()))
                mweeks = self.weeksleftmonth()
                return mstoreweek!
            }
            
        }
        throw BudgetError.FailedToCalculateBudget
        
    }
    
    /// returns the Budget amount for the current month for a budget, 99% of the time this will be for the active budget
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    /// - throws: A `BudgetError.FailedToCalculateBudget` error, if the budget could not be calculated
    /// - returns: Int: the calculated amount per month for the budget
    func getBudgetAmount(ForMonth index:Int) throws -> Float{
        if self.BudgetList[index].week == true {
            return ((self.BudgetList[index].amount_week * 4) + (self.BudgetList[index].increments - self.BudgetList[index].deductions))
        }
        else if self.BudgetList[index].month == true{
            return (self.BudgetList[index].amount_month + self.BudgetList[index].increments - self.BudgetList[index].deductions)
        }
        throw BudgetError.FailedToCalculateBudget
    }
    
    
    
    /// adds a deduction for a budget
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    ///   - Float: The amount that should be deducted
    func DeductBudget(index:Int, amount:Float) throws {
        
        if amount < 0  {
            throw BudgetError.InvlaidAmount
        }
        
        
        //make local change
        self.BudgetList[index].deductions += amount
        
        //make changes to core data
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
        let info = self.fetchedResultsController.fetchedObjects?.count
        
        for var index = 0; index < info; index++ {
            if info == 0 {
                break
            }
            let result = self.fetchedResultsController.fetchedObjects![index]
            var deductions:Float?
            var active:Bool?
            
            if let BudgetDeductions = result.valueForKey("deductions") as? Float {
                deductions = BudgetDeductions
            }
            if let BudgetActive = result.valueForKey("active") as? Bool {
                active = BudgetActive
            }
            if active == true {
                print(deductions)
                deductions = deductions! + amount
                result.setValue(deductions, forKey: "deductions")
                do {
                    try self.managedObjectContext?.save()
                    self.fetch()
                    self.loadArray()
                } catch {}
                
            }
            
        }
    }
    
    /// adds an increment for a budget
    /// - parameters:
    ///   - Int: The index at which the budget is stored in the BudgetList
    ///   - Float: The amount that should be added to increments
    func IncreaseBudget(index:Int, amount:Float) throws {
        
        if amount < 0  {
            throw BudgetError.InvlaidAmount
        }
        
        //make change localy
        self.BudgetList[index].increments += amount
        
        
        //make changes to core data
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
        let info = self.fetchedResultsController.fetchedObjects?.count
        
        for var index = 0; index < info; index++ {
            if info == 0 {
                break
            }
            let result = self.fetchedResultsController.fetchedObjects![index]
            var increments:Float?
            var active:Bool?
            
            if let BudgetIncrements = result.valueForKey("increments") as? Float {
                increments = BudgetIncrements
            }
            if let BudgetActive = result.valueForKey("active") as? Bool {
                active = BudgetActive
            }
            if active == true {
                increments = increments! + amount
                result.setValue(increments, forKey: "increments")
                do {
                    try self.managedObjectContext?.save()
                    self.fetch()
                    self.loadArray()
                } catch {}
                
            }
            
        }
        
       
    }
    
}

