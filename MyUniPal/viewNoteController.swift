//
//  viewNoteController.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 12/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class viewNoteController: UIViewController , UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate, UITextViewDelegate {
    

    @IBOutlet weak var noteTitle: UITextField!

    
    @IBOutlet weak var text: UITextView!
    
    
    
    @IBOutlet weak var imageview: UIImageView!
   
  
    
    @IBOutlet weak var camerabutton: UIButton!
    
    
    
    var imagePicker: UIImagePickerController!
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    //passed through
    var note:Note?
    var index:Int?
    var new:Bool?
    var newnote:Note?
    
    //constructed here
    var isimage:Bool?
    var image:UIImage?
    
    
    
    
    @IBAction func camera_pressed(sender: UIButton) {
        //////Error-Dialog//////
        print("About Dialog Shown")
        let title = NSLocalizedString("Switching to image", comment: " ")
        let message = NSLocalizedString("Are you sure you would like to discard your text and use an image as a note instead?", comment: " ")
        let doneTitle = NSLocalizedString("Cancel", comment: "label on button used to dismiss the dialog")
        let yesTitle = NSLocalizedString("Yes", comment: "label on button used to dismiss the dialog")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: doneTitle, style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: yesTitle, style: UIAlertActionStyle.Default, handler: {(action) -> Void in
          self.isimage = true
            self.text.hidden = true
            self.imageview.hidden = false
            
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
            
            
            
            
            }
            
            ))
        presentViewController(alert, animated: true, completion: nil)
    ///////////////////End-of-About-Dialog///////////
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.isimage = false
        self.text.hidden = false
        self.imageview.hidden = true
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imageview.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        
        imageview.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.image!.size)
        
        
        scrollview.contentSize = image!.size
        
       
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewDoubleTapped:")
        doubleTapRecognizer.numberOfTapsRequired = 2
        doubleTapRecognizer.numberOfTouchesRequired = 1
        self.scrollview.addGestureRecognizer(doubleTapRecognizer)
        
        
        let scrollViewFrame = self.scrollview.frame
        let scaleWidth = scrollViewFrame.size.width / self.scrollview.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / self.scrollview.contentSize.height
        let minScale = min(scaleWidth, scaleHeight) * 6 ;
        self.scrollview.minimumZoomScale = minScale;
        
        
        self.scrollview.maximumZoomScale = 31.0
        scrollview.zoomScale = minScale;
        self.scrollview.backgroundColor = UIColor.clearColor()
        
        
        centerScrollViewContents()

        
        
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
    
    func save() {
        
        if new == true {
            do {
                
                if isimage == false {
                    let image:UIImage = UIImage(named: "finance_icon")!
                    let data:NSData = UIImagePNGRepresentation(image)!
                    self.newnote = Note(title: self.noteTitle.text!, image: data, text: self.text.text, isimage: false)
                }
                else {
                    self.newnote = Note(title: self.noteTitle.text!, image: UIImagePNGRepresentation(self.image!)!, text: " ", isimage: true)
                }
            
            try notes.getInstance.saveNote(newnote!)
            NSNotificationCenter.defaultCenter().postNotificationName("reloadNotes", object: nil)
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2])!, animated: true)
            }
            catch {
                //////Error-Dialog//////
                print("About Dialog Shown")
                let title = NSLocalizedString("Save Error", comment: " ")
                let message = NSLocalizedString("The note title cannot be empty when saving. Please add a note title", comment: " ")
                let doneTitle = NSLocalizedString("Done", comment: "label on button used to dismiss the dialog")
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: doneTitle, style: UIAlertActionStyle.Cancel, handler: nil))
                presentViewController(alert, animated: true, completion: nil)
            }
            ///////////////////End-of-About-Dialog///////////

            
        }
        else {
            do {
                
             if isimage == false {
                let image:UIImage = UIImage(named: "finance_icon")!
                let data:NSData = UIImagePNGRepresentation(image)!
                self.newnote = Note(title: self.noteTitle.text!, image: data, text: self.text.text!, isimage: false) }
             else {
                self.newnote = Note(title: self.noteTitle.text!, image: UIImagePNGRepresentation(self.image!)!, text: " ", isimage: true)
                }
            try notes.getInstance.updateNote(index!, note: newnote!)
                NSNotificationCenter.defaultCenter().postNotificationName("reloadNotes", object: nil)
            
             self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2])!, animated: true)
            } catch {
                
            }
        }
    }
    
    ///implemented by folling the tutorial at http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
    func centerScrollViewContents() {
        let boundsSize = self.scrollview.bounds.size
        var contentsFrame = self.imageview.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        self.imageview.frame = contentsFrame
    }
    
    ///implemented by folling the tutorial at http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
    func scrollViewDoubleTapped(recognizer: UITapGestureRecognizer) {
        
        let pointInView = recognizer.locationInView(self.imageview)
        
        
        var newZoomScale = self.scrollview.zoomScale * 1.5
        newZoomScale = min(newZoomScale, self.scrollview.maximumZoomScale)
        
       
        let scrollViewSize = self.scrollview.bounds.size
        let w = scrollViewSize.width / newZoomScale
        let h = scrollViewSize.height / newZoomScale
        let x = pointInView.x - (w / 2.0)
        let y = pointInView.y - (h / 2.0)
        
        let rectToZoomTo = CGRectMake(x, y, w, h);
        
        
        self.scrollview.zoomToRect(rectToZoomTo, animated: true)
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        
            return self.imageview
      
        
        
    }
    ///implemented by folling the tutorial at http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
    func scrollViewDidZoom(scrollView: UIScrollView) {
        if self.isimage == true{
            centerScrollViewContents()
        }
        else{
            
        }
        
    }
    
        
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        imagePicker =  UIImagePickerController()
        self.noteTitle.delegate = self
        self.scrollview.delegate = self
        self.imagePicker.delegate = self
        self.text.delegate = self
        imagePicker.sourceType = .Camera
        
        if self.isimage == true {
            self.text.hidden = true
            self.imageview.hidden = false
            self.image = UIImage(data: (note?.image)!)
            self.image = UIImage(CGImage: self.image!.CGImage!, scale: 1, orientation: UIImageOrientation.Right)
            self.imageview.image = self.image
            imageview.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.image!.size)
            
            // 2
            scrollview.contentSize = image!.size
            
            // 3
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewDoubleTapped:")
            doubleTapRecognizer.numberOfTapsRequired = 2
            doubleTapRecognizer.numberOfTouchesRequired = 1
            self.scrollview.addGestureRecognizer(doubleTapRecognizer)
            
            // 4
            let scrollViewFrame = self.scrollview.frame
            let scaleWidth = scrollViewFrame.size.width / self.scrollview.contentSize.width
            let scaleHeight = scrollViewFrame.size.height / self.scrollview.contentSize.height
            let minScale = min(scaleWidth, scaleHeight) * 7 ;
            self.scrollview.minimumZoomScale = minScale;
            
            // 5
            self.scrollview.maximumZoomScale = 2.0
            scrollview.zoomScale = minScale;
            
            // 6
            centerScrollViewContents()
            
        } else {
            self.text.hidden = false
            self.imageview.hidden = true
            
            
            self.scrollview.backgroundColor = UIColor(patternImage: UIImage(named: "paperbackground")!)
            
            self.text.userInteractionEnabled = true
            
            self.text.scrollEnabled = true
            
         
            
            
            
        }
        
        let savebutton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("save"))
        let updatebutton = UIBarButtonItem(title: "Update", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("save"))
        if new == true{
            self.navigationItem.rightBarButtonItem = savebutton
        }
        else {
            self.navigationItem.rightBarButtonItem = updatebutton
        }
        

        
        
        
        if (new == false){
            self.title = note!.title
            self.new = false
            self.noteTitle.text = note!.title
            self.text.text = note!.text
            
            
        }
        else {
            self.title = "New Note"
            self.new = true
            self.noteTitle.placeholder = "Enter name for new note"
        }
        
    }
    
    //if user touches outside the textbox
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //end editing (dismiss keyboard)
        self.view.endEditing(true)
    }

    //what happens when the return key is pressed
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
}