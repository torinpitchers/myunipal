//
//  deals.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 01/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import SystemConfiguration

struct Deal {
    var title:String
    var image:NSData
    var link:String
    var description:String
    var category_name:String
    var submit_time:String
    var timestamp_date:Int
    var temperature:Float
    var merchant_name:String
    var price:Float
    var expired:Bool

}

enum JSONError: ErrorType {
    case InvalidURL(String)
    case InvalidKey(String)
    case InvalidArray
    case InvalidData
    case InvalidImage
    case indexOutOfRange
}

enum fetchError: ErrorType {
    case IndexOutOfRange
}


class Deals: NSObject, NSFetchedResultsControllerDelegate {
    
    private var dealsArray:[Deal]
    
    // static class public property that returns a Budget object
    static let getInstance = Deals()
    

    /// creates a new Budget object from within the class only
    private override init() {
        // initialises the empty array
        self.dealsArray = []
    }
    
    
    /// fetches the data from core data, usualy done before caling <b>loadArray()</b>
    /// - throws: A `fetchError.IndexOutOfRange` error, if the index was not found
    /// - returns: NSManagedObject: the object(deal) at that index
    func fetchDeal(index:Int) throws -> NSManagedObject {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Deal")
        
        var returning: NSManagedObject?
        //3
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if index >= results.count {
                throw fetchError.IndexOutOfRange
            }
            
            let Dealz = results as! [NSManagedObject]
            
            returning = Dealz[index]
            return returning!
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        throw fetchError.IndexOutOfRange
    }
    
    /// clears all deals from coredata
    func clear() {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Deal")
        
        //3
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let Dealz = results as! [NSManagedObject]
            for current_deal in Dealz {
                managedContext.deleteObject(current_deal)
                try managedContext.save()
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    /// returns the count of the deals both local and coredata
    /// - returns: Int: the count of Deals
    func count() -> Int {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Deal")
        
        var returning:Int = 0
        //3
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let Dealz = results as! [NSManagedObject]
            returning = Dealz.count
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        
        return returning
        
    }
    

    /// saves a deal to coredata
    /// - parameters:
    ///   - Deal: The deal to be save to core data
       func saveDeal(deal:Deal) {
        /* before using core data you need to get a reference to a 'NSManagedObjectContext' which is used to work with managed objects before they get saved to disk. This can represent any entity in the data model. */
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        /* the NSEntityDescription links the entity definition from the data model to the NSManagedObject. */
        let entity = NSEntityDescription.entityForName("Deal", inManagedObjectContext: managedContext)
        /* the NSManagedObject implements the functionality required to create a Core Data model object. */
        let newDeal = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        /* now we can assign values to the data model object keys */
        newDeal.setValue(deal.title, forKey: "title")
        newDeal.setValue(deal.image, forKey: "image")
        newDeal.setValue(deal.link, forKey: "link")
        newDeal.setValue(deal.description, forKey: "desc")
        newDeal.setValue(deal.category_name, forKey: "category_name")
        newDeal.setValue(deal.submit_time, forKey: "submit_time")
        newDeal.setValue(deal.timestamp_date, forKey: "timestamp_date")
        newDeal.setValue(deal.temperature, forKey: "temperature")
        newDeal.setValue(deal.merchant_name, forKey: "merchant_name")
        newDeal.setValue(deal.price, forKey: "price")
        newDeal.setValue(deal.expired, forKey: "expired")
        do {
            /* finally we try to save the data model object. This may throw an error (a generic NSError). */
            try managedContext.save()
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
    }

    
    /// loads deals from json using a recursive function to caluclate how many pages you want when called.
    /// - parameters:
    ///   - Pages: amount of pages of deals to load to device
    /// - throws: `JSONError.InvalidURL()` error, if the url is invalid ,    `JSONError.InvalidKey()` error, if the key fails,    `JSONError.InvalidData` error, if the data is invalid
        func load(pages:Int) throws {
        if pages < 1 {
            return
        }
        
        
        var jsonUrl = "http://api.hotukdeals.com/rest_api/v2/?key=6402409879f15bcc35676f8a2a3c0170&output=json&results_per_page=30&min_temp=500&order=hot&page="
        jsonUrl = jsonUrl + String(pages)
        let session = NSURLSession.sharedSession()
        guard let dealsUrl = NSURL(string: jsonUrl) else {
            throw JSONError.InvalidURL(jsonUrl)
        }
        session.dataTaskWithURL(dealsUrl, completionHandler: {(data, response, error) -> Void in
           
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                guard let deals:[AnyObject] = (((json["deals"])!!["items"]) as! [AnyObject]) else {
                    throw JSONError.InvalidArray
                }
                for deal in deals {
                    guard let title:String = deal["title"] as? String else {
                        throw JSONError.InvalidKey("title")
                    }
                    guard let image_string:String = deal["deal_image"] as? String else {
                        throw JSONError.InvalidKey("deal_image")
                    }
                    guard let image_url = NSURL(string: image_string) else {
                        throw JSONError.InvalidURL(image_string)
                    }
                    guard let image = NSData(contentsOfURL: image_url) else {
                        throw JSONError.InvalidData
                    }
                    guard let link:String = deal["deal_link"] as? String else {
                        throw JSONError.InvalidKey("deal_link")
                    }
                    guard let description:String = deal["description"] as? String else {
                        throw JSONError.InvalidKey("description")
                    }
                    guard let category:AnyObject = deal["category"] else {
                        throw JSONError.InvalidKey("category")
                    }
                    guard let category_name:String = category["name"] as? String else {
                        throw JSONError.InvalidKey("category_name")
                    }
                    guard let timestamp_date:Int = deal["timestamp"] as? Int else {
                        throw JSONError.InvalidKey("submit_time")
                    }
                    guard let submit_time:String = deal["submit_time"] as? String else {
                        throw JSONError.InvalidData
                    }
                    guard let temperature:Float = deal["temperature"] as? Float else {
                        throw JSONError.InvalidKey("temperature")
                    }
                    guard let merchant:AnyObject = deal["merchant"] else {
                        throw JSONError.InvalidKey("merchant")
                    }
                    guard let merchant_name:String = merchant["name"] as? String else {
                        throw JSONError.InvalidKey("name")
                    }
                    guard let price_data:AnyObject = deal["price"] else {
                        throw JSONError.InvalidKey("price")
                    }
                    var price:Float?
                    if let price_null:NSNull = price_data as? NSNull {
                        price = 0
                    }
                    else {
                        guard let price_string:String = price_data as! String else {
                            throw JSONError.InvalidKey("price")
                        }
                        if let price_check:Float = Float(price_string) {
                            price = price_check
                            
                        } else {
                            price = 0
                        }

                    }
                    guard let expired_string:String = deal["expired"] as? String else {
                        throw JSONError.InvalidKey("expired")
                    }
                    var expired:Bool?
                    if expired_string == "true" {
                        expired = true
                    } else if expired_string == "false" {
                        expired = false
                    }
                    let newDeal = Deal(title: title, image: image, link: link, description: description, category_name: category_name, submit_time: submit_time, timestamp_date: timestamp_date, temperature: temperature, merchant_name: merchant_name, price: price!, expired: expired!)
                    if expired == false || price == 0{
                        
                        self.saveDeal(newDeal)
                    }
                }
            } catch {
                print("Fetch failed: \((error as! NSError).localizedDescription)")
            }
            //completion(deals)
        }).resume()
        
        try load(pages - 1)
        
    }
    
    
    /// reorders deals stored in coredata
    /// - parameters:
    ///   - String: The key by which to sort the data
    ///   - Bool: sort by ascending or not
    func reorder(key:String, ascending:Bool) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Deal")
        let sortTitle = NSSortDescriptor(key: key, ascending: ascending)
        
        
        fetchRequest.sortDescriptors = [sortTitle]
        
        print("got")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let Dealz = results as! [NSManagedObject]
            for deal in Dealz {
                let newdeal = Deal(title: deal.valueForKey("title") as! String, image: deal.valueForKey("image") as! NSData, link: deal.valueForKey("link") as! String, description: deal.valueForKey("desc") as! String, category_name: deal.valueForKey("category_name") as! String, submit_time: deal.valueForKey("submit_time") as! String,timestamp_date: deal.valueForKey("timestamp_date") as! Int, temperature: deal.valueForKey("temperature") as! Float, merchant_name: deal.valueForKey("merchant_name") as! String, price: deal.valueForKey("price") as! Float, expired: deal.valueForKey("expired") as! Bool)
                managedContext.deleteObject(deal)
                self.saveDeal(newdeal)
            }
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        
    }
    
    /// returns the status of the devices interent connection capabilites
    /// - returns: Bool: true if internet connection avaliable, false if not
    func checkconnection()-> Bool {
        var ConnectionStatus:Bool = false
        let URL = NSURL(string: "http://google.com/")
        let req = NSMutableURLRequest(URL: URL!)
        req.timeoutInterval = 5.0
        
        var response: NSURLResponse?
        
        do{
            _ = try NSURLConnection.sendSynchronousRequest(req, returningResponse: &response) as NSData? }
        catch{}
        
        if let Response = response as? NSHTTPURLResponse {
            if Response.statusCode == 200 {
                ConnectionStatus = true
            }
        }
        
        return ConnectionStatus
        
    }
    
    
    

}