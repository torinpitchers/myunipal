//
//  notes.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 12/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import Foundation
import CoreData
import UIKit


struct Note {
    var title:String
    var image:NSData
    var text:String
    var isimage:Bool
    
}

enum NoteError: ErrorType {
    case NoteNotExists
    case NoteGetFailed
    case NotGetCount
    case NameEmpty
    case BadIndex
    
    
}


class notes: NSObject, NSFetchedResultsControllerDelegate {
    
    static let getInstance = notes()
    
    
    
    /// saves a note to coredata
    /// - parameters:
    ///   - Note: The note to be save to core data
    func saveNote(note: Note) throws{
        print("saving note")
        if note.title == "" || note.title == " " {
            throw NoteError.NameEmpty
        }
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appdelegate.managedObjectContext
        let entity = NSEntityDescription.entityForName("Note", inManagedObjectContext: managedcontext)
        let newNote = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedcontext)
        
        newNote.setValue(note.title, forKey: "title")
        newNote.setValue(note.image, forKey: "image")
        newNote.setValue(note.text, forKey: "text")
        newNote.setValue(note.isimage, forKey: "isimage")
        
        do {
            try managedcontext.save()
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
        
    }
    
    func updateNote(index:Int, note:Note) throws {
        print("saving note")
        if note.title == "" || note.title == " " {
            throw NoteError.NameEmpty
        }
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appdelegate.managedObjectContext
        let req = NSFetchRequest(entityName: "Note")
        
        do {
            let results:[NSManagedObject] = try managedcontext.executeFetchRequest(req) as! [NSManagedObject]
            let object:NSManagedObject = results[index] 
            object.setValue(note.title, forKey: "title")
            object.setValue(note.text, forKey: "text")
            object.setValue(note.image, forKey: "image")
            object.setValue(note.isimage, forKey: "isimage")
            try managedcontext.save()
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
        
    }
    

    
    
    func getNote(index:Int) throws -> Note {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appdelegate.managedObjectContext
        let req = NSFetchRequest(entityName: "Note")
        do {
            let results:[NSManagedObject] = try managedcontext.executeFetchRequest(req) as! [NSManagedObject]
            
            
            let formatted_note = Note(title: (results[index].valueForKey("title") as! String), image: (results[index].valueForKey("image") as! NSData), text: (results[index].valueForKey("text") as! String), isimage: (results[index].valueForKey("isimage") as! Bool))
            return formatted_note
            
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
            throw NoteError.NoteGetFailed
        }
        
    }
    
    
    
    
    func deleteNote(index:Int) -> Void {
       
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appdelegate.managedObjectContext
        let req = NSFetchRequest(entityName: "Note")
        do {
            let results:[NSManagedObject] = try managedcontext.executeFetchRequest(req) as! [NSManagedObject]
            managedcontext.deleteObject(results[index])
            try managedcontext.save()
            
            
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
        
    }
    
    func count()  -> Int {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedcontext = appdelegate.managedObjectContext
        let req = NSFetchRequest(entityName: "Note")
        var count:Int = 0
        do {
            let results:[NSManagedObject] = try managedcontext.executeFetchRequest(req) as! [NSManagedObject]
            count = results.count
            
        } catch let error as NSError {
            print("an error ocurred: \(error.userInfo)")
        }
        return count
    }
    
    func clear() {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appdelegate.managedObjectContext
        let req:NSFetchRequest = NSFetchRequest(entityName: "Note")
        do {
            let objects: [NSManagedObject] = try managedContext.executeFetchRequest(req) as! [NSManagedObject]
            for note in objects {
                managedContext.deleteObject(note)
            }
            try managedContext.save()
        } catch {}
    }
    
    
}