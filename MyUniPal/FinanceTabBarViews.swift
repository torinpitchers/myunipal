//
//  FinanceTabViewController.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 09/11/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation




/// The **FinanceTabViewController** class handles the tab views for the **Finance manager* tool.
///
/// Overview
///
/// The controller initialises the tabs and sets icons and titles
class FinanceTabViewController: UITabBarController {
    
    
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        //set view title
        self.title = "Finance manager"
        
        //setting titles of the tabbar items programmaticly
        let tabitems = self.tabBar.items
        let tabbar1 = tabitems![0]
        let tabbar2 = tabitems![1]
        tabbar1.title = "My Allowance"
        tabbar2.title = "My Budgets"
        tabbar1.image = UIImage(named: "tab1")!
        tabbar2.image = UIImage(named: "tab2")!
        /////////
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
/// The **FinanceViewController1** class handles the UI for the **My Allowance* tab within the financial manager tool.
///
/// Overview
///
/// The controller provides support for the following activities:
///
/// 1. converting Floats to Strings that can be displayed in the UI
/// 2. converting Strings to Floats specificaly used for text input from the user
/// 3. updating the UI labels
/// 4. Handeling UI changed such as Segment selection & button pressing
/// 5. Initialising the UI for that tab
class FinanceViewController1: UIViewController,UIAlertViewDelegate,UIGestureRecognizerDelegate {
    
    var audioPlayer = AVAudioPlayer()
    
    //defining errors
    enum UIError: ErrorType {
        case FailedToUpdateAmountLabel
    }
    
    //including the constraint
    @IBOutlet weak var lowerlabelconstraint: NSLayoutConstraint!
    
    //top labels
    @IBOutlet weak var BudgetLabel: UILabel!
    @IBOutlet weak var leftthis: UILabel!
    //segment
    @IBOutlet weak internal var TimeControl: UISegmentedControl!
    
    //bottom buttons
    @IBOutlet weak var spendbutton: UIButton!
    @IBOutlet weak var receivebutton: UIButton!
    
    //progressbar
    @IBOutlet weak var progressbar: UIProgressView!
    @IBOutlet weak var progresslabel: UILabel!
    
    
    
    /// constructs a formatted string, ready for display on the UI.
    /// - parameters:
    ///   - Float: The amount which is to be formatted to a string
    func stringforamount(amount:Float) -> String{
        //if the amount to be displayed is above or equal to 4 digits
        if amount >= 1000 {
            BudgetLabel.font = UIFont(name: "Avenir Next Condensed", size: 75.0)
            //move the lowerlabel up to meet the font
            lowerlabelconstraint.constant = 100
        }
       //if its below 4 digits
        else {
            BudgetLabel.font = UIFont(name: "Avenir Next Condensed", size: 90.0)
            //keep the lower label at 110
            lowerlabelconstraint.constant = 110
        }
        //create the string and return it
        let budgetstring:String = String(format:"%.2f", amount)
        return ("£" + budgetstring)
    }
    
    /// converts a string to a Float.
    /// - parameters:
    ///   - String: The tex requiring conversion
    func stringtofloat(text: String) -> Float {
        if let integer:Float = Float(text) {
            return integer
        }
        return Float(0)
    }
    
    
    ///Function that is responsible for updating the UILabels correctly for the active budget.
    /// The function checks what segment is active from the segmentcontroler and updates the labels accordingly
    ///the UILabeles are then updated to reflect this change
    /// - parameters:
    ///   - UIButton: The Button that has been pressed
    func updatelabelamount() throws {
        
        progressbar.hidden = false
        progresslabel.hidden = false
        TimeControl.hidden = false
        spendbutton.hidden = false
        receivebutton.hidden = false
        
        do {
            //get the active budget index, if not throw an error
            let ActiveBudgetIndex = try Budget.getInstance.getBudgetActiveIndex()
            //use the budget index to try get the amount, if not throw an error
            var BudgetAmount:Float = 0
            
            
            //switch to check which state of the segment so we know how to update the label
            switch TimeControl.selectedSegmentIndex
            {
            //if day segment is chosen
            case 0:
                do {
                    //use the budget index to try get the amount for day, if not throw an error
                    BudgetAmount = try Budget.getInstance.getBudgetAmount(ForDay: ActiveBudgetIndex)
                    self.leftthis.text = "Left Today"
                    //if the amount returned is less than 0 then show the label as 0
                    if BudgetAmount < 0 {
                        BudgetAmount = 0
                    }
                        
                    
                    
                    /////progressbar///////
                    let unit:Float = 1.0 / 24
                    let progress:Float = 1.0 - (unit * Float(Budget.getInstance.hoursinday()))
                    self.progressbar.progress = progress
                    let string:String = String(Budget.getInstance.hoursinday()) + " Hours left to go!"
                    self.progresslabel.text = string
                    
                    print("Label for Day successfully updated")
                }
                catch BudgetError.FailedToCalculateBudget{ print("Error Budget amount could not be calculated for day") }
            //if week segment is chosen
            case 1:
                do {
                    //use the budget index to try get the amount, if not throw an error
                    BudgetAmount = try Budget.getInstance.getBudgetAmount(ForWeek: ActiveBudgetIndex)
                    leftthis.text = "Left this Week"
                    
                    //if the amount returned is less than 0 then show the label as 0
                    if BudgetAmount < 0 {
                        BudgetAmount = 0
                    }
                    
                    /////progressbar///////
                    let unit:Float = 1.0 / 4.0
                    let progress:Float = 1.0 - (unit * Float(Budget.getInstance.weeksleftmonth()))
                    self.progressbar.progress = progress
                    let string:String = String(Budget.getInstance.weeksleftmonth()) + " Weeks left to go!"
                    self.progresslabel.text = string

                    
                    print("Labels for Week successfully updated")
                }
                catch BudgetError.FailedToCalculateBudget{ print("Error Budget amount could not be calculated for week") }
            //if month segment is chosen
            case 2:
                do {
                    //use the budget index to try get the amount, if not throw an error
                    BudgetAmount = try Budget.getInstance.getBudgetAmount(ForMonth: ActiveBudgetIndex)
                    leftthis.text = "Left This Month"
                    
                    /////progressbar///////
                    let unit:Float = 1.0 / Float(Budget.getInstance.daysinmonth())
                    let progress:Float = 1.0 - (unit * Float(Budget.getInstance.daysleftmonth()))
                    self.progressbar.progress = progress
                    let string:String = String(Budget.getInstance.daysleftmonth()) + " Days left to go!"
                    self.progresslabel.text = string
                    
                    print("Labels for Month successfully updated")
                }
                catch BudgetError.FailedToCalculateBudget{ print("Error Budget amount could not be calculated for month") }
            default:
                break
            } //<- end switch
            
           self.BudgetLabel.text = self.stringforamount(BudgetAmount)
        } catch BudgetError.ActiveNotFound {
            print("Error Active Budget has not been found")
            
            BudgetLabel.font = UIFont(name: "Avenir Next Condensed", size: 50.0)
            BudgetLabel.text = "No Budgets"
            leftthis.text = "Please activate a budget"
            progressbar.hidden = true
            progresslabel.hidden = true
            TimeControl.hidden = true
            spendbutton.hidden = true
            receivebutton.hidden = true
        }
        
        throw UIError.FailedToUpdateAmountLabel
    }
    
    


    func swipeLeft(recognizer : UISwipeGestureRecognizer) {
        print("swiped left")
        self.dismissViewControllerAnimated(true, completion: nil)
        
        self.tabBarController?.selectedIndex = 1
        
    }
    func swipeRight(recognizer : UISwipeGestureRecognizer) {
        print("swiped right")
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func swipeLeftBudget(recognizer : UISwipeGestureRecognizer) {
        print("swiped left on Budget Label")
        switch TimeControl.selectedSegmentIndex
        {
            //if day segment is chosen
        case 0:
            TimeControl.selectedSegmentIndex = 1
            do {
                try self.updatelabelamount()
            }
            catch{}
            
            
        case 1:
            TimeControl.selectedSegmentIndex = 2
            do {
                try self.updatelabelamount()
            }
            catch{}
            
            
        case 2:
            break
            
        default:
            break
        }

    }
    
    func swipeRightBudget(recognizer : UISwipeGestureRecognizer) {
        print("swiped right on Budget Label")
        switch TimeControl.selectedSegmentIndex
        {
            //if day segment is chosen
        case 0:
            break
            
            
        case 1:
            TimeControl.selectedSegmentIndex = 0
            do {
                try self.updatelabelamount()
            }
            catch{}
            
            
        case 2:
            TimeControl.selectedSegmentIndex = 1
            do {
                try self.updatelabelamount()
            }
            catch{}
            
        default:
            break
        }
        
    }

    
    
    
    
    
    /// Action Function that is responsible for handling the day/week/month segmented control, specifically when the selection is changed.
    /// The function updates the UILabels that are appropriate for that selection.
    /// - parameters:
    ///   - UISegmentedControl: The segmented control that has been changed
    @IBAction func indexchange(sender: UISegmentedControl) {
        
        do{
        //switch to check which state of the segment
        switch TimeControl.selectedSegmentIndex
        {
        //if day segment is chosen
        case 0:
            //output the selection to the console
            print("Day Selected")
            try updatelabelamount()                //change the lower label to the appriate string for today
           
        //if week segment is chosen
        case 1:
            print("week Selected")
            try updatelabelamount()                //change the lower label to the appriate string for today
            
        //if month segment is chosen
        case 2:
            print("month Selected")
            try updatelabelamount()                //change the lower label to the appriate string for today
            
        default:
            break } //<- end switch
        } catch {
        
        }
    } //<- end action
    
    
    /// Action Function that is responsible for handling when the spend button is pressed.
    /// The function presents a UIAlert to the user for them to input the amount spent. If amount is entered this is then deducted from their active budget balance
    ///the UILabeles are then updated to reflect this change
    /// - parameters:
    ///   - UIButton: The Button that has been pressed
    @IBAction func spendpressed(sender: UIButton) {
    
        
    //////spend-Dialog//////
        print("Spend Dialog Shown")
        let title = NSLocalizedString("Spend Money", comment: " ")
        let message = NSLocalizedString("How much money have you spent?", comment: "instructions on how to enter a spending")
        let addTitle = NSLocalizedString("Spend", comment: "label on button to make the spend")
        let cancelTitle = NSLocalizedString("Cancel", comment: "label on button used to cancel and dismiss the dialog")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler(nil)
        
        alert.addAction(UIAlertAction(title: addTitle, style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            
            let item:String = alert.textFields![0].text!
            let itemLength:Int = item.characters.count
            if (itemLength >= 1) {
                
                //create string from the input
                let inputval = self.stringtofloat(item)
                do{
                    //get the active budget index
                    let ActiveBudgetIndex = try Budget.getInstance.getBudgetActiveIndex()
                    //make deduction to budget
                    try Budget.getInstance.DeductBudget(ActiveBudgetIndex, amount: inputval)
                    print("deduction made")
            
                    
                    
                    //try update the labels
                    self.audioPlayer.play()
                    try self.updatelabelamount()

                } catch  { }
            } //-> end if statement
        })) //-> end handler
        
        alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
        ///////////////////End-of-Spend-Dialog///////////
    
    
    
    
    /// Action Function that is responsible for handling when the receive button is pressed.
    /// The function presents a UIAlert to the user for them to input the amount received. If amount is entered this is then added to their active budget balance
    ///the UILabeles are then updated to reflect this change
    /// - parameters:
    ///   - UIButton: The Button that has been pressed
    @IBAction func receivepressed(sender: UIButton) {
        
        //////Receive-Dialog//////
        print("Receive Dialog Shown")
        
        let title = NSLocalizedString("Receive Money", comment: " ")
        let message = NSLocalizedString("How much money have you received?", comment: "instructions on how to enter a receiving")
        let addTitle = NSLocalizedString("Receive", comment: "label on button to make the spend")
        let cancelTitle = NSLocalizedString("Cancel", comment: "label on button used to cancel and dismiss the dialog")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler(nil)
        
        alert.addAction(UIAlertAction(title: addTitle, style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            
            let item:String = alert.textFields![0].text!
            
            let itemLength:Int = item.characters.count
            if (itemLength >= 1) {
                
                //create string from the input
                let inputval = self.stringtofloat(item)
                do{
                    //get the active budget index
                    let ActiveBudgetIndex = try Budget.getInstance.getBudgetActiveIndex()
                    //make deduction to budget
                    try Budget.getInstance.IncreaseBudget(ActiveBudgetIndex, amount: inputval)
                    print("increase made")
                    
                 
                    
                    
                    //try update the labels
                    self.audioPlayer.play()
                    try self.updatelabelamount()
                    
                } catch  { }
            } //-> end if statement
        })) //-> end handler
        
        alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
        
        ///////////////////End-of-Receive-Dialog///////////
    }
    

    func initbackground() {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.drawInRect(self.view.bounds)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        self.view.backgroundColor = UIColor(patternImage: image)

    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        let device:UIDevice = UIDevice.currentDevice()
        let orientation: UIDeviceOrientation = device.orientation
        //if orientation has changed to portrait
        if orientation == UIDeviceOrientation.Portrait {
            super.performSelector("initbackground", withObject: nil, afterDelay: 0.1)
            print("Changed to Portrait")
        }
            //if orientation has changed to landscape
        else if orientation == UIDeviceOrientation.LandscapeLeft || orientation == UIDeviceOrientation.LandscapeRight {
            //self.viewDidLoad()
            //reinitialise background after a slight delay as this method is called moments before transition has occured
            super.performSelector("initbackground", withObject: nil, afterDelay: 0.1)
            print("Changed to landscape")
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        //setting background image
        initbackground()

        //inititalising segment colors
        TimeControl.backgroundColor = ConsistentColorAero
        
        
        //button layout setup
        spendbutton.layer.borderWidth = 1.0
        receivebutton.layer.borderWidth = 1.0
        spendbutton.contentEdgeInsets = UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0)
        receivebutton.contentEdgeInsets = UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0)
        spendbutton.layer.borderColor = UIColor.blackColor().CGColor
        receivebutton.layer.borderColor = UIColor.blackColor().CGColor
        spendbutton.layer.backgroundColor = ConsistentColorAero.CGColor
        receivebutton.layer.backgroundColor = ConsistentColorAero.CGColor
        
        //segment layout setup
        TimeControl.layer.borderColor = ConsistentColorAero.CGColor
        TimeControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        TimeControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Selected)
        TimeControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGrayColor()], forState: UIControlState.Highlighted)
        TimeControl.tintColor = UIColor.blackColor()
        
        //setting progress bar color
        progressbar.tintColor = ConsistentColorAero
        
        //gestures for view
        //left
        let recognizer_left: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeLeft:")
        recognizer_left.direction = .Left
        //right
        let recognizer_right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRight:")
        recognizer_right.direction = .Right
        
        //add gestures
        self.view.addGestureRecognizer(recognizer_left)
        self.view.addGestureRecognizer(recognizer_right)
        
        
        //label gestures
        BudgetLabel.userInteractionEnabled = true
        
        //left
        let Budget_left: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeLeftBudget:")
        Budget_left.direction = .Left
        
        //right
        let Budget_right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRightBudget:")
        Budget_right.direction = .Right
        
        //add
        BudgetLabel.addGestureRecognizer(Budget_left)
        BudgetLabel.addGestureRecognizer(Budget_right)
        
        //sounds
        
        

        let coinSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("moneySound", ofType: "mp3")!)
        
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOfURL: coinSound) } catch {}
        
        self.audioPlayer.prepareToPlay()
        
        
        
        
        
        


    }
    
    override func viewDidAppear(animated: Bool) {
        
        //if its a new month we need to reset all budgets 
        do {
            
            //dates
            let active_date = try Budget.getInstance.getBudget(ByIndex: Budget.getInstance.getBudgetActiveIndex()).activedate
            let today_date = NSDate()
            //calendar object
            let calendar = NSCalendar.currentCalendar()
            
            //calulate month for dates
            let active_month = calendar.component(NSCalendarUnit.Month, fromDate: active_date)
            let today_month = calendar.component(NSCalendarUnit.Month, fromDate: today_date)
            if today_month != active_month {
                
                Budget.getInstance.reset()
            }
        }
        catch {}
        
        //setting background image
        initbackground()

        //everytime the view will load we need to reload the data and update labels
        Budget.getInstance.fetch()
        Budget.getInstance.loadArray()
        do {
            try self.updatelabelamount()
        }
        catch{}
        
    }
    
    override func viewWillAppear(animated: Bool) {
        Budget.getInstance.loadArray()
        do {
            try self.updatelabelamount()
        } catch{}
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


/// The **FinanceViewController2** class handles the UI for the **My Budgets* tab within the financial manager tool.
///
/// Overview
///
/// The controller provides support for the following activities:
///
/// 1. converting Floats to Strings that can be displayed in the UI
/// 2. converting Strings to Floats specificaly used for text input from the user
/// 3. updating the UI labels
/// 4. Handeling UI changed such as Segment selection & button pressing
/// 5. Initialising the UI for that tab

class FinanceViewController2: UITableViewController {
    
    func swipeRight(recognizer : UISwipeGestureRecognizer) {
        print("swiped right")
        self.dismissViewControllerAnimated(true, completion: nil)
        self.tabBarController?.selectedIndex = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setting title
        self.title = "My Budgets"
        
        //gestures
        //right
        let recognizer_right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRight:")
        recognizer_right.direction = .Right
        
        //add gestures
        self.view.addGestureRecognizer(recognizer_right)
        
        print("ViewDidLoad - setup complete")
        
        

    }
    
    override func viewWillAppear(animated: Bool) {
        //everytime the view will load we need to construct a new array that holds any new budgets added then reload the table
        Budget.getInstance.fetch()
        Budget.getInstance.loadArray()
        print("viewWillAppear - Fetched data")
    }
    
    override func viewDidAppear(animated: Bool) {
        /* each time the view becomes visible we reload the table view */
        self.tableView.reloadData()
        print("ViewDidAppear - table reloaded")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //telling tableview how many sections I would like,  I want two one for the active budget another for the rest
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //telling table view how many rows are to be expected
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //amount of table rows should be the amount of tools avaliable plus an extra row for tghe add button
        return (Budget.getInstance.count() + 1)
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }

    

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row > 0 {
        if editingStyle == .Delete {
            print("Delete option for cell selected")
            
            //Delete the row from the data source
            do {try Budget.getInstance.deleteBudget(indexPath.row-1)
               // print("cell deleted")
            }
            catch{}
            Budget.getInstance.fetch()
            Budget.getInstance.loadArray()
            tableView.reloadData()
            
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        }
    }

    
    
    //function that responds to the activator switch being changed]
    func activatorswitched(activator: UISwitch) {
        
        if activator.on == true {
            print("switched on")
            print(activator.tag)
            Budget.getInstance.PreviousActiveOff()
            do{
                try Budget.getInstance.newactive(activator.tag)
            }
            catch{}
            
            Budget.getInstance.called = false
            Budget.getInstance.called2 = false
            Budget.getInstance.mcalled = false
            Budget.getInstance.mcalled2 = false
            Budget.getInstance.fetch()
            Budget.getInstance.loadArray()
            self.tableView.reloadData()
            
            
            
        }
        else {
            print("switched off")
            print(activator.tag)
            Budget.getInstance.PreviousActiveOff()
            Budget.getInstance.fetch()
            Budget.getInstance.loadArray()
            self.tableView.reloadData()
        }
        
    }
    
    
    
    //creating the cells in tableview
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        Budget.getInstance.fetch()
        Budget.getInstance.loadArray()
        //create a cell for the index path
        let cell = tableView.dequeueReusableCellWithIdentifier("budget_option", forIndexPath: indexPath)
        do {
            //if the cell is for a budget
            if indexPath.row == 0
            {
                if let lastcell:UITableViewCell = cell {
                    lastcell.textLabel!.text = "Add Budget"
                    lastcell.textLabel!.textAlignment = NSTextAlignment.Center
                    lastcell.backgroundColor = ConsistentColor
                }}
                    
            //if its the last cell
            else {
                let budget = try Budget.getInstance.getBudget(ByIndex: indexPath.row - 1)
                
                //create switch
                let activator: UISwitch = UISwitch()
                //adding tag to regonise which cell the switch belongs to
                activator.tag = indexPath.row
                activator.addTarget(self, action: "activatorswitched:", forControlEvents: UIControlEvents.ValueChanged)
                //if budget is active one
                if budget.active == true {
                    activator.on = true
                }
                //add switch to table
                cell.accessoryView = activator
                
                if let label = cell.textLabel {
                    //if its the active budget
                    if budget.active == true {
                        label.font = UIFont.boldSystemFontOfSize(22.0)
                        label.textColor = ConsistentColor
                        
                    }
                    else {
                        label.textColor = UIColor.blackColor()
                        label.font = UIFont.systemFontOfSize(18.0)
                    }
                    
                     /*if budget.month == true {
                        let string: String = budget.name + ": " + String(format:"%.2f", budget.amount_month)
                        label.text = string
                    }
                    else{
                        let string: String = budget.name + ": " + String(format:"%.2f", budget.amount_week)
                        label.text = string
                    } */
                    
                    label.text = budget.name
                    
                    
                }
            }
            
            
            
                }
            
    catch {}
    
return cell
}
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //if the cell selected is the fist one (Deals Tool)
        if indexPath == NSIndexPath(forRow: 0, inSection: 0) {
            //actvate segue for the add button
            self.performSegueWithIdentifier("AddStart", sender: self)
            print("Add Budget Selected")
            
            
        }
    }
    
}

class Addbudget1: UIViewController {
    
    //conneting ui buttons
    @IBOutlet weak var weekly: UIButton!
    @IBOutlet weak var monthly: UIButton!
    
    override func viewDidLoad() {
        //initialising buttons
        weekly.sizeThatFits(CGSize(width: 560, height: 92))
        monthly.sizeThatFits(CGSize(width: 560, height: 92))
        weekly.layer.backgroundColor = ConsistentColor.CGColor
        monthly.layer.backgroundColor = ConsistentColor.CGColor
    }
    
    
    @IBAction func weeklypressed(sender: UIButton) {
        Budget.getInstance.week = true
        Budget.getInstance.month = false
        self.performSegueWithIdentifier("AddSecond", sender: self)
        
    }
    
    @IBAction func monthlypressed(sender: UIButton) {
        Budget.getInstance.week = false
        Budget.getInstance.month = true
        self.performSegueWithIdentifier("AddSecond", sender: self)
    }
    
    
}

class Addbudget2: UIViewController,UITextFieldDelegate {
    
    //conneting ui buttons
 
    @IBOutlet weak var mylabel: UILabel!
    
    @IBOutlet weak var nextbutton: UIButton!
    
    @IBOutlet weak var name: UITextField!
    
    override func viewDidLoad() {
        nextbutton.layer.backgroundColor = ConsistentColor.CGColor
        self.name.delegate = self
    }
    
    @IBAction func nameadded(sender: UIButton) {
        if name.text == "" || name.text == " " {
            //////Error-Dialog//////
            print("Error Dialog Shown")
            let title = NSLocalizedString("Name Error", comment: " ")
            let message = NSLocalizedString("The budget title cannot be empty. Please add a title", comment: " ")
            let doneTitle = NSLocalizedString("Done", comment: "label on button used to dismiss the dialog")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: doneTitle, style: UIAlertActionStyle.Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }
        ///////////////////End-of-About-Dialog///////////
        else{
    
        Budget.getInstance.name = name.text!
        self.performSegueWithIdentifier("AddThird", sender: self)
        }
    }
    //what happens when the return key is pressed
   func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    //if user touches outside the textbox
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //end editing (dismiss keyboard)
        self.view.endEditing(true)
    }
    
    
    
}

class Addbudget3: UIViewController, UITextFieldDelegate {
    
    
    //linking the ui switch to determin if the budget to be added should be set as the active budget
    @IBOutlet weak var active: UISwitch!
    //linking the add budget button to finish adding the budget
    @IBOutlet weak var addbutton: UIButton!
    //linking the amount text field, to specify the amount to add
    @IBOutlet weak var amount: UITextField!
    
    override func viewDidLoad() {
        //button styles
        addbutton.layer.backgroundColor = ConsistentColor.CGColor
        //setting delegate
        self.amount.delegate = self
        
    }

    //action for when the button is pressed
    @IBAction func finished(sender: UIButton) {
        if amount.text == "" || amount.text == " "{
            //////Error-Dialog//////
            print("Error Dialog Shown")
            let title = NSLocalizedString("Amount Error", comment: " ")
            let message = NSLocalizedString("The budget amount cannot be empty. Please add an amount", comment: " ")
            let doneTitle = NSLocalizedString("Done", comment: "label on button used to dismiss the dialog")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: doneTitle, style: UIAlertActionStyle.Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            ///////////////////End-of-About-Dialog///////////
        }
        else{
            
            if Budget.getInstance.week == true {
                Budget.getInstance.amount_week = Budget.getInstance.stringtofloat(amount.text!)
                Budget.getInstance.amount_month = 0.0
            }
            else{
                Budget.getInstance.amount_week = 0.0
                Budget.getInstance.amount_month = Budget.getInstance.stringtofloat(amount.text!)
                
            }
            if active.on == true {
                Budget.getInstance.active = true
            }
            else {
                Budget.getInstance.active = false
            }
            
            
            let budgettoadd = BudgetType(name: Budget.getInstance.name!, amount_week: Budget.getInstance.amount_week!, amount_month: Budget.getInstance.amount_month!, deductions: 0.0, increments: 0.0, week: Budget.getInstance.week!, month: Budget.getInstance.month!, active: Budget.getInstance.active!, activedate: NSDate())
            
            if budgettoadd.active == true {
                Budget.getInstance.PreviousActiveOff()
                
            }
            
            //notify that active budget has changed
            Budget.getInstance.activechangedday = true
            Budget.getInstance.activechangedweek = true
            
            //try Budget.getInstance.addBudget(budgettoadd)
            Budget.getInstance.saveBudget(budgettoadd)
            
            
            
            //self.navigationController?.popToRootViewControllerAnimated(true)
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
            
        }
        }
    //if user touches outside the textbox
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //end editing (dismiss keyboard)
        self.view.endEditing(true)
    }
    //what happens when the return key is pressed
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
    }
    




    