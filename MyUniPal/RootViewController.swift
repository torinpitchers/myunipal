//
//  RootViewController.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 06/11/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication
import AudioToolbox

//creating a structure that will define tha data required for a tool
struct tools {
    var name:String
    var icon:UIImage
}


//creating tools
let tool1 = tools(name: "Hot Deals", icon: UIImage(named: "deals_icon")!)
let tool2 = tools(name: "Note Taker", icon: UIImage(named: "note_icon")!)
let tool3 = tools(name: "Finance Manager", icon: UIImage(named: "finance_icon")!)

//initialisng and populating the array to store what tools are avaliable for the user
let tool_list:[tools] = [tool1, tool2, tool3]

//UIcommoncolor
let ConsistentColorAero = UIColor(red: 0x1A / 255.0, green: 0xD6 / 255.0, blue: 0xFD / 255.0, alpha: 0.3)
let ConsistentColor = UIColor(red: 0x1A / 255.0, green: 0xD6 / 255.0, blue: 0xFD / 255.0, alpha: 1)

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


class MenuController: UITableViewController, UIAlertViewDelegate {
    
    //alert for touch id
    func IDAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    

    //linking the about button
    @IBOutlet weak var about: UIBarButtonItem!
    //function to rspond to when the button is pressed
    @IBAction func about_pressed(sender: UIBarButtonItem) {
        
  
        //////About-Dialog//////
        print("About Dialog Shown")
        let title = NSLocalizedString("About MyUniPal", comment: " ")
        let message = NSLocalizedString("Created by Torin Pitchers at Coventry University.", comment: "adding about information")
        let doneTitle = NSLocalizedString("Done", comment: "label on button used to dismiss the dialog")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: doneTitle, style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    ///////////////////End-of-About-Dialog///////////
    

    //private variable to count the rows to provide different background colors in the cells
    private var rowcount = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "MyUniPal"
    
        
        
        //only load deals if no deals are saved and have a valid internet connection
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        do {
            let count = Deals.getInstance.count()
            //if no deals have been loaded
            if count  < 100 {
                //if valid internet connection
                if Deals.getInstance.checkconnection() {
                    //try load the deals
                    
                    try Deals.getInstance.load(6)
                    
                }
                
            }
            
        } catch{}
        self.tableView.reloadData()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        
        //load array on first load
        Budget.getInstance.loadArray()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //telling tableview how many sections I would like,  I only want one
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //telling table view how many rows are to be expected
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //amount of table rows should be the amount of tools avaliable
        return tool_list.count
    }
    
    //specifying the height for the cells
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let viewBounds = self.view.bounds
        let navigationBounds = self.navigationController?.navigationBar.bounds
        var TableHeight = viewBounds.height - (navigationBounds?.height)!
        
        let device:UIDevice = UIDevice.currentDevice()
        let orientation: UIDeviceOrientation = device.orientation
        //if orientation has changed to portrait
        if orientation == UIDeviceOrientation.Portrait {
            TableHeight -= 20
            return (TableHeight / CGFloat(tool_list.count))
           
        }
            //if orientation has changed to landscape
        else if orientation == UIDeviceOrientation.LandscapeLeft || orientation == UIDeviceOrientation.LandscapeRight {
            return TableHeight / CGFloat(tool_list.count)
        }

        return TableHeight / CGFloat(tool_list.count)

    }
    
    
    //creating the cells in tableview
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("tool_option", forIndexPath: indexPath)
        
        //optional (stops a crash)
        if let label = cell.textLabel {
            //set the label equal to the name of the tool
            label.text = tool_list[indexPath.row].name
            label.font = UIFont(name: "Copperplate", size: 20.0)
            
        }
        if let image = cell.imageView {
            //set the label icon to the corresponding icon
            image.image = tool_list[indexPath.row].icon
            image.frame = CGRectOffset(cell.frame, 250.0, 25.0)
            
        }
        
        
        //print current row count to the console
        print(rowcount)
        //only set the colour for the first run (static colour not dynamic) otherwise the rows will change when you scoll
        if rowcount <= tool_list.count - 1{
            //if the rowcount is even
            if self.rowcount%2 == 0 {
                //make cell background color cyan
                cell.backgroundColor = ConsistentColor
                
            } else {
                //otherwise make it white
                cell.backgroundColor = UIColor.whiteColor()
            }
            //increment row count
            rowcount += 1
        }
        
        
        return cell
    }
    
    //when a cell is delected
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //if the cell selected is the fist one (Deals Tool)
        if indexPath == NSIndexPath(forRow: 0, inSection: 0) {
            //actvate segue for the deals view
            self.performSegueWithIdentifier("ToDeals", sender: self)
            print("Tool Hot Deals selected")
            
            //if the cell selected is the fourth one (Finance Manager Tool)
        } else if indexPath == NSIndexPath(forRow: 1, inSection: 0) {
            print("Tool Note Taker selected")
            self.performSegueWithIdentifier("ToNotes", sender: self)
        }
            
            
        else if indexPath == NSIndexPath(forRow: 2, inSection: 0) {
            print("Tool Finance Manager selected")
            
            //verify touch id
            // 1
            let context = LAContext()
            var error: NSError?
            
            // 2
            // check if Touch ID is available
            if context.canEvaluatePolicy(.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
                // 3
                let reason = "Authenticate with Touch ID"
                context.evaluatePolicy(.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
                    {(succes: Bool, error: NSError?) -> Void in
                        // 4
                        if succes {
                            //perform on main queue
                            dispatch_async(dispatch_get_main_queue(), {
                                self.performSegueWithIdentifier("ToFinance", sender: self)
                                
                            })
                            
                        }
                        else {
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                //vibrate
                                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                                self.IDAlertController("Touch ID Authentication Failed")
                                
                            })
                        }
                        
                })
                
            }
                // 5
            else {
                //if no touch id just perform the segue
                self.performSegueWithIdentifier("ToFinance", sender: self)
            }
 
        }
    }
    
    
}






