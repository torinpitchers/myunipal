//
//  NotesTableViewController.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 12/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import UIKit



class NotesTableViewController: UITableViewController {
    
    func reloadNotes(notification: NSNotification){
        //load data here
        self.tableView.reloadData()
    }

    
    func click_add() {
        self.performSegueWithIdentifier("ViewNotesAdd", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Note Taker"
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        let addbutton = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("click_add"))
        self.navigationItem.rightBarButtonItem = addbutton
        
        //add notification for reloading data
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadNotes:",name:"reloadNotes", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var count:Int = 0
        count = notes.getInstance.count()
      
        return count
    }
    
    
   override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("note_cell", forIndexPath: indexPath)
        do {
            let note:Note = try notes.getInstance.getNote(indexPath.row)
            
            if note.isimage == true {
                if let imageview = cell.imageView {
                    imageview.image = UIImage(named: "imageTableIcon")
                }
            }
            else {
                if let imageview = cell.imageView {
                    imageview.image = UIImage(named: "noteTableIcon")
                }
            }
            
            if let label = cell.textLabel {
                label.text = note.title
            }
            
            
            
        } catch{}
    cell.editing = true
        return cell
    }
    
    
    
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            
           notes.getInstance.deleteNote(indexPath.row)
           
            
            
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    
    

    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if (segue.identifier == "ViewNotes")  {
    do{
    let controller = segue.destinationViewController  as! viewNoteController
    var note:Note?
    
    if let row:Int = (sender as! NSIndexPath).row {
    note = try notes.getInstance.getNote(row)
    controller.index = row
    }
    
    controller.note = note
    controller.new = false
        
        if note?.isimage == true {
            controller.isimage = true
        }
        else {
            controller.isimage = false
        }
    
    }
    catch{}
    }
        
        
    if (segue.identifier == "ViewNotesAdd")  {
        let controller = segue.destinationViewController  as! viewNoteController
        let note:Note? = Note(title: "", image: NSData(), text: "", isimage: false)
        controller.note = note
        controller.new = true
        controller.isimage = false
        
        }
    
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier("ViewNotes", sender: indexPath)
        
            

        
    }
    
    
}



