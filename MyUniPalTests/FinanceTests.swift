//
//  MyUniPalTests.swift
//  MyUniPalTests
//
//  Created by Torin Pitchers on 06/11/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import XCTest
@testable import MyUniPal

class FinanceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        Budget.getInstance.clear()
        Budget.getInstance.removedata()
        let budget1 = BudgetType(name: "budget1", amount_week: 0, amount_month: 500, deductions: 100, increments: 100, week: false, month: true, active: false, activedate: NSDate())
        let budget2 = BudgetType(name: "budget2", amount_week: 200, amount_month: 0, deductions: 0, increments: 0, week: true, month: false, active: true, activedate: NSDate())
        let budget3 = BudgetType(name: "budget3", amount_week: 0, amount_month: 1000, deductions: 0, increments: 0, week: false, month: true, active: false, activedate: NSDate())
        let budget4 = BudgetType(name: "budget4", amount_week: 300, amount_month: 0, deductions: 0, increments: 0, week: true, month: false, active: false, activedate: NSDate())
        
        Budget.getInstance.saveBudget(budget1)
        Budget.getInstance.saveBudget(budget2)
        Budget.getInstance.saveBudget(budget3)
        Budget.getInstance.saveBudget(budget4)
    }
    
    func test_count() {
        XCTAssertEqual(Budget.getInstance.count(), 4)
    }
    
    func test_general() {
        
        let days = Budget.getInstance.daysinmonth()
        let hours = Budget.getInstance.hoursinday()
        let float = Budget.getInstance.stringtofloat("0.00")
        XCTAssertEqual(float, 0.00)
        
        
    }
    
    
    func test_reset() {
        do {
            let oldbudget = try Budget.getInstance.getBudget(ByIndex: 0)
            let oldincre = oldbudget.increments
            Budget.getInstance.reset()
            Budget.getInstance.fetch()
            Budget.getInstance.loadArray()
            let newbudget = try Budget.getInstance.getBudget(ByIndex: 0)
            let newincre = newbudget.increments
        
            XCTAssertLessThan(newincre, oldincre) }
        catch {
        XCTFail()
        }
        
        
       
    }

    
    
    func test_getting_active_index_active_budget_present() {
        do{
            let index = try Budget.getInstance.getBudgetActiveIndex()
            //active index should be 1
            XCTAssertEqual(index, 1)
            
        }
        catch {XCTFail("Should not be an error")}
    }
    
    func test_getting_active_index_active_budget_NOTpresent() {
        do{
            //switching the active budget(budget2) off
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            Budget.getInstance.managedObjectContext = appDelegate.managedObjectContext
            
            let budget = Budget.getInstance.fetchedResultsController.fetchedObjects![1]
            budget.setValue(false, forKey: "active")
            
            try Budget.getInstance.managedObjectContext?.save()
            
            Budget.getInstance.fetch()
            Budget.getInstance.loadArray()
            
            //attempting to get the active index when we know there now is none
            _ = try Budget.getInstance.getBudgetActiveIndex()
            XCTFail("Should not reach this line")
        }
        catch BudgetError.ActiveNotFound {
            //should catch this error
            XCTAssert(true)
        }
        catch{
        XCTFail("Should not be any other error caught")
        }
    }
    
    func test_getting_budget_index_good() {
        do {
            let budget = try Budget.getInstance.getBudget(ByIndex: 3)
            XCTAssertEqual(budget.name, "budget4")
        }
        catch{
            XCTFail("Should not be an error")
        }
    }
    
    func test_getting_budget_index_bad() {
        do {
            _ = try Budget.getInstance.getBudget(ByIndex: 5)
             XCTFail("Should not reach this line")
        }
        catch BudgetError.BadIndex {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }

    }
    
    func test_deleting_budget_index_good() {
        
        do{
            try Budget.getInstance.deleteBudget(3)
            XCTAssertEqual(Budget.getInstance.count(), 4)
        }
        catch{
            XCTFail("Should not be an error")
        }
        
    }
    
    func test_deleting_budget_index_bad() {
        
        do{
            try Budget.getInstance.deleteBudget(5)
            XCTFail("Should not reach this line")
        }
        catch BudgetError.BadIndex {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }

        
    }
    
    
    
    func test_getting_amount_day() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            let amount = try Budget.getInstance.getBudgetAmount(ForDay: index)
        
            XCTAssertEqual(amount, 800.0 / Float(Budget.getInstance.daysleftmonth()))
        }
        catch {
            XCTFail("Should not be an error")
        }
    }
    
    func test_getting_amount_week() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            let amount = try Budget.getInstance.getBudgetAmount(ForWeek: index)
            
            XCTAssertEqual(amount, 800.0 / Float(Budget.getInstance.weeksleftmonth()))
        }
        catch {
            XCTFail("Should not be an error")
        }
    }
    
    func test_getting_amount_month() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            let amount = try Budget.getInstance.getBudgetAmount(ForMonth: index)
            
            XCTAssertEqual(amount, 800)
        }
        catch {
            XCTFail("Should not be an error")
        }
    }
    
    
    
    
    func test_increase() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            try Budget.getInstance.IncreaseBudget(index, amount: 10)
            let amount = try Budget.getInstance.getBudgetAmount(ForMonth: index)
            
            XCTAssertEqual(amount, 800 + 10)
        }
        catch {
            XCTFail("Should not be an error")
        }
    }
    
    func test_increase_Invalid() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            try Budget.getInstance.IncreaseBudget(index, amount: -10)
            XCTFail("Should not reach this line")
        }
        catch BudgetError.InvlaidAmount {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }
    }
    
        func test_deduction() {
            do {
                let index = try Budget.getInstance.getBudgetActiveIndex()
                try Budget.getInstance.DeductBudget(index, amount: 10)
                let amount = try Budget.getInstance.getBudgetAmount(ForMonth: index)
                
                XCTAssertEqual(amount, 800 - 10)
            }
            catch {
                XCTFail("Should not be an error")
            }
    }
    
    func test_deduction_Invalid() {
        do {
            let index = try Budget.getInstance.getBudgetActiveIndex()
            try Budget.getInstance.DeductBudget(index, amount: -10)
            XCTFail("Should not reach this line")
        }
        catch BudgetError.InvlaidAmount {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }
    }
    
    func test_newactive() {
        do {
            try Budget.getInstance.PreviousActiveOff()
            try Budget.getInstance.newactive(3)
            try Budget.getInstance.fetch()
            try Budget.getInstance.loadArray()
            let active = try Budget.getInstance.getBudgetActiveIndex()
            
            XCTAssertEqual(active, 2)
        }
        catch {
            XCTFail("Should not be an error")
        }
    }
    
    
    

    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
