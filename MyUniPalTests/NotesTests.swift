//
//  NotesTests.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 15/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import XCTest
@testable import MyUniPal


class NotesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        notes.getInstance.clear()
        let image:UIImage = UIImage(named: "finance_icon")!
        let data:NSData = UIImagePNGRepresentation(image)!
        let note1 = Note(title: "note1", image: data, text: "note text", isimage: false)
        let note2 = Note(title: "note2", image: data, text: "note text", isimage: true)
        var count:Int!
        do {
            try notes.getInstance.saveNote(note1)
            try notes.getInstance.saveNote(note2)
            count = try notes.getInstance.count()
        } catch{ XCTFail()}
        
        XCTAssertEqual(count, 2)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testdeleteGood() {
        
        var count:Int!
        do {
            try notes.getInstance.deleteNote(0)
            count = try notes.getInstance.count()
        } catch{XCTFail()}
        
        XCTAssertEqual(count, 1)

        
    }
    
    func testSaveEmpty() {
        let image:UIImage = UIImage(named: "finance_icon")!
        let data:NSData = UIImagePNGRepresentation(image)!
        let note3 = Note(title: "", image: data, text: "note text", isimage: true)
        do{
            try notes.getInstance.saveNote(note3)
        XCTFail()
        }
        catch NoteError.NameEmpty {
            XCTAssertTrue(true)
        } catch {XCTFail()}
    }
    
    
    func testGetGood() {
        do {
            let note = try notes.getInstance.getNote(1)
            XCTAssertEqual(note.title, "note2")
        } catch{XCTFail()}
        
        
    }
    
        
    func testUpdate() {
        do {
            let image:UIImage = UIImage(named: "finance_icon")!
            let data:NSData = UIImagePNGRepresentation(image)!
            let note = Note(title: "note1UPDATE", image: data, text: "UPDATED", isimage: true)
            try notes.getInstance.updateNote(0, note: note)
            
            let note1 = try notes.getInstance.getNote(0)
            
            XCTAssertNotEqual(note1.title, "note1")
            XCTAssertNotEqual(note1.isimage, false)
        } catch{XCTFail()}
        
        
    }
    
    func testUpdateEmpty() {
    
        do{
            let image:UIImage = UIImage(named: "finance_icon")!
            let data:NSData = UIImagePNGRepresentation(image)!
            let note = Note(title: " ", image: data, text: "UPDATED", isimage: true)
            try notes.getInstance.updateNote(0, note: note)
            
            let note1 = try notes.getInstance.getNote(0)
            XCTFail("Should not reach this line")
        }
        catch NoteError.NameEmpty {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }

  
    
}
}
