//
//  Dealstests.swift
//  MyUniPal
//
//  Created by Torin Pitchers on 10/12/2015.
//  Copyright © 2015 Torin Pitchers. All rights reserved.
//

import XCTest
@testable import MyUniPal
import CoreData

class Dealstests: XCTestCase {
    
    override func setUp() {
      


    }
    
    
    func test_CheckConnection() {
        
        let connection:Bool = Deals.getInstance.checkconnection()
        XCTAssertEqual(connection, true)
    }
    
    func test_fetchBad() {
        
        do {
            _ = try Deals.getInstance.fetchDeal(500)
            XCTFail("Should not reach this line")
        }
        catch fetchError.IndexOutOfRange {
            //should catch this error
            XCTAssert(true)
        }
        catch{
            XCTFail("Should not be any other error caught")
        }
    }
    
    func test_loadGoodTest() {
        
        do {
            Deals.getInstance.clear()
            try Deals.getInstance.load(6)
            
            sleep(20)
            let count = try Deals.getInstance.count()
            XCTAssertGreaterThan(count, 1)
        }
        catch{
            XCTFail()
        
        }
        
        
    }
    

    
    func testreorder() {
        do {
            sleep(30)
            Deals.getInstance.reorder("price", ascending: true)
            sleep(5)
            let deal5 = try Deals.getInstance.fetchDeal(4) as! NSManagedObject
            let deal30 = try Deals.getInstance.fetchDeal(29)
            
            let price5:Float = deal5.valueForKey("price") as! Float
            let price30:Float = deal30.valueForKey("price") as! Float
            
            XCTAssertLessThan(price5, price30)
        }
        catch{
            XCTFail()
            
        }
    }
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
