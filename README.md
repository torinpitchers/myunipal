# Description #

MyUniPal is an IOS application developed using swift for apple devices. The app aims to be a companion app for university students assisting them with their daily activities and tries to make their life easier by providing tools such as:

* Hot Deals
* Note Taker
* Finance Manager


## Hot Deals ##

Downloads the best and latest UK deals from the UK Hot Deals api (http://www.hotukdeals.com/rest-api). The deals can then be viewed and reorder where appropriate for the user to view specific deals they want. The user can then view and get these deals. All deals are stored locally so they can be viewed in the future without an internet connection

## Note Taker ##

Enables the user to store notes locally and access them whenever they need. This feature is envisioned to be used for storing lecture notes in an organised manner digitally. A user can also take a picture of a physical written note and store this as a note so they have a consistent note store.  

## Finance Manager ##

Allows the user to set themselves budgets and then calculates an allowance per day, week and month. the user can then add their spending or receiving of money and keep within their set budget. The budget manager tries to not be obstructive and allows a user to spend over their budget if they really want to.